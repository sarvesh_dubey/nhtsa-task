import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment'
import {
  HttpClientModule,
  HttpClient,
  HttpRequest,
  HttpResponse,
  HttpEventType,
  HttpErrorResponse
} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MainServiceService {

  constructor( private http: HttpClient) { }

  getInputData(data){
    let Url =environment.baseUrl+data+'?format=json';
    return this.http.get(Url);
  }
}
