import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewMainComponentComponent } from './view-main-component.component';

describe('ViewMainComponentComponent', () => {
  let component: ViewMainComponentComponent;
  let fixture: ComponentFixture<ViewMainComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewMainComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewMainComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
