import { Component, OnInit } from '@angular/core';
import {MainServiceService} from '../../service/main-service.service';
@Component({
  selector: 'app-view-main-component',
  templateUrl: './view-main-component.component.html',
  styleUrls: ['./view-main-component.component.scss']
})
export class ViewMainComponentComponent implements OnInit {
  constructor(public mainServiceService:MainServiceService) { }
  listOfData:any;
  searchNameInput:any;
  showLoader:boolean = false;
  error:any;
  checkClearText:boolean = false;
  ngOnInit(): void {

  }
  submitText(data):void{
    this.searchNameInput = data;
    this.showLoader = true;
    this.mainServiceService.getInputData(data).subscribe((res:any)=>{
      this.showLoader = false;
      if(res){
        this.listOfData = res;
      }
    },(err) => {
      this.showLoader = false;
      this.error = err;
    })
  }

  clearData():void{
    this.checkClearText = !this.checkClearText;
    if(this.listOfData){
      this.listOfData.Results = [];
    }

  }
}
