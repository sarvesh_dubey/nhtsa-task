import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinderViewComponentComponent } from './finder-view-component.component';

describe('FinderViewComponentComponent', () => {
  let component: FinderViewComponentComponent;
  let fixture: ComponentFixture<FinderViewComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinderViewComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinderViewComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
