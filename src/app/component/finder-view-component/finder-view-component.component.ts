import { Component, OnInit, Output,EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-finder-view-component',
  templateUrl: './finder-view-component.component.html',
  styleUrls: ['./finder-view-component.component.scss']
})
export class FinderViewComponentComponent implements OnInit {
  public finderFormGroup: FormGroup;
  _checkClearText:any;
  @Input() error;
  public makeName;
  @Output() getTextData  = new EventEmitter();
  @Input ('checkClearText')
  set checkClearText(val){
    this.resetForm()
    this._checkClearText= val;
  }
  get checkClearText(){
    return this._checkClearText;
  }
  constructor() { }

  ngOnInit(): void {
    this.finderFormGroup = new FormGroup({
      makeName : new FormControl('', [Validators.required,Validators.pattern(/^[\w\s]+$/)])
    });
  }
  public checkError = (controlName: string, errorName: string) => {
    return this.finderFormGroup.controls[controlName].hasError(errorName);
  }
  onSubmit():void{
    if(this.finderFormGroup && this.finderFormGroup.value && this.finderFormGroup.value.makeName){
      let checkString = this.finderFormGroup.value.makeName.replace(/\s/g, "").toLowerCase();
      if(checkString.length > 0){
        this.getTextData.emit(this.finderFormGroup.value.makeName);
      }else{
        this.resetForm();
        alert('Please enter something');
      }
    }
  }

  resetForm():void{
    this.makeName = '';
    if(this.finderFormGroup){
      if(this.finderFormGroup.value && this.finderFormGroup.value.makeName){
        this.finderFormGroup.value.makeName = '';
      }

      this.finderFormGroup.reset();
    }
  }
}
