import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-card-view-component',
  templateUrl: './card-view-component.component.html',
  styleUrls: ['./card-view-component.component.scss']
})
export class CardViewComponentComponent implements OnInit {
  _listOfData:any;
  _searchName:any;
  @Input('getData')
  set getData(value) {
    this._listOfData = value;
  }
  get getData() {
    return this._listOfData;
  }
  @Input('searchName')
  set searchName(value) {
    this._searchName = value;
  }
  get searchName() {
    return this._searchName;
  }
  constructor() { }

  ngOnInit(): void {
  }
}
